[![License](https://img.shields.io/badge/licens-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/eheimwbn/droplistsmgmt)](https://goreportcard.com/report/gitlab.com/eheimwbn/droplistsmgmt)
[![Build](https://gitlab.com/eheimwbn/droplistsmgmt/badges/master/build.svg)](https://gitlab.com/eheimwbn/droplistsmgmt/pipelines)
[![GoDoc](https://godoc.org/gitlab.com/eheimwbn/droplistsmgmt?status.svg)](https://godoc.org/gitlab.com/eheimwbn/droplistsmgmt)
# get SPAMHAUSE DROP and EDROP lists
