package utests

import (
	"fmt"
	"gitlab.com/eheimwbn/droplistsmgmt/pkg/spamhaus"
	"testing"
)

func TestGetSpamhause(t *testing.T) {
	result := spamhause.GetSpamhause("https://www.spamhaus.org/drop/drop.txt")
	if len(result) <= 5 {
		t.Errorf("GetSpamhause was incorrect, got: length of %d.", len(result))
	}
	fmt.Printf("TestGetSpamhause OK\n")
}
