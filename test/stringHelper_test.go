package utests

import (
	"fmt"
	"gitlab.com/eheimwbn/droplistsmgmt/pkg/stringHelper"
	"testing"
)

func TestIsStringInSlice(t *testing.T) {
	word := "Bert"
	wordSlice := []string{"Bert", "Ernie", "Samson"}
	hit := stringHelper.IsStringInSlice(word, wordSlice)
	if hit != true {
		t.Errorf("IsStringInSlice was incorrect, got: %t, want: %s.", hit, "true")
	}
	fmt.Printf("IsStringInSlice OK \n")
}

func TestIsStringInSliceNegative(t *testing.T) {
	word := "Lilo"
	wordSlice := []string{"Bert", "Ernie", "Samson"}
	hit := stringHelper.IsStringInSlice(word, wordSlice)
	if hit == true {
		t.Errorf("IsStringInSliceNegative was incorrect, got: %t, want: %s.", hit, "false")
	}
	fmt.Printf("IsStringInSliceNegative OK\n")
}
