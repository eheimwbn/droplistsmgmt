package stringHelper

// Find string in array (slice) return true if so else return false
func IsStringInSlice(word string, wordlist []string) bool {
	for _, field := range wordlist {
		if field == word {
			return true
		}
	}
	return false
}
