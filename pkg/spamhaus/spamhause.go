package spamhause

import (
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"
)

// fetch spamhause droplist return ip-net slice
func GetSpamhause(list string) []string {
	ipv4NetRegex := regexp.MustCompile(`([0-9]{1,3}\.){3}[0-9]{1,3}/\d+`)

	// Create HTTP client with timeout
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	// Get spamhause drop
	response, err := client.Get(list)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Get the response body as a string
	dataInBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	pageContent := string(dataInBytes)
	contentSlice := strings.Split(pageContent, "\n")

	var s []string
	for _, net := range contentSlice {
		s = append(s, ipv4NetRegex.FindString(net))
	}
	return s

}
